import logging

import requests
from requests import codes
from settings import settings
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, constants
from telegram.ext import (
    ApplicationBuilder,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    MessageHandler,
    filters,
)

logging.basicConfig(
    level=logging.DEBUG if settings.debug else logging.INFO,
    format="%(levelname)s %(asctime)s %(name)s %(message)s",
)
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

if settings.bot_sentry_dsn:
    import sentry_sdk

    sentry_sdk.init(
        dsn=settings.bot_sentry_dsn,
        traces_sample_rate=1.0,
        profiles_sample_rate=1.0,
        environment=settings.environment,
    )

NO = "no"


async def get_user_by_code(confirmation_code):
    logger.info(
        "Fetching user details from the auth service using code %s", confirmation_code
    )
    result = requests.get(
        f"{settings.auth_service_base_url}/v1/code/{confirmation_code}/",
        headers={
            settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
        },
        timeout=10,
    )
    if not result.ok:
        logger.error(
            "There's an error fetching user from the auth service using code %s. "
            "Response %s",
            confirmation_code,
            result.content.decode(),
        )
        return None
    data = result.json()
    logger.info("Got user details by code %s, user %s", confirmation_code, data)
    return data


async def confirm_user_using_code(confirmation_code, telegram_id):
    logger.info(
        "Confirming user account details using code %s, telegram id %s",
        confirmation_code,
        telegram_id,
    )
    result = requests.post(
        f"{settings.auth_service_base_url}/v1/code/confirm/",
        json={
            "code": confirmation_code,
            "telegram_id": str(telegram_id),
        },
        headers={
            settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
        },
        timeout=10,
    )
    error_message = ""
    if not result.ok:
        logger.error(
            "Cannot confirm user details using code %s, telegram id %s. Response %s",
            confirmation_code,
            telegram_id,
            result.json(),
        )
        if result.status_code == codes.conflict:
            error_message = "Аккаунт с таким Телеграм уже зарегистрирован"
        else:
            error_message = (
                "Что-то пошло не так, ваш аккаунт не подтвержден, "
                "отправьте код ещё раз."
            )
    return result.ok, error_message


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text = update.message.text
    logger.info("Got /start message with text %s", text)
    try:
        _, confirmation_code = text.split()
    except ValueError:
        confirmation_code = None

    if confirmation_code:
        return await code(update, context, confirmation_code)

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=settings.welcome_text,
    )


async def code(
    update: Update, context: ContextTypes.DEFAULT_TYPE, confirmation_code=None
):
    confirmation_code = confirmation_code or update.message.text
    logger.info(
        "Getting confirmation code %s, text %s", confirmation_code, update.message.text
    )
    user_details = await get_user_by_code(confirmation_code)

    if not user_details:
        logger.info("Cannot fetch user details using code %s", confirmation_code)
        await update.message.reply_text(
            "Произошла ошибка, скорее всего вы отправили недействительный код."
        )
        return

    keyboard = [
        [
            InlineKeyboardButton("Да", callback_data=confirmation_code),
            InlineKeyboardButton("Нет", callback_data=NO),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    logger.info(
        "Found user %s using code %s", user_details["username"], confirmation_code
    )
    await update.message.reply_text(
        f"Вы действительно хотите подтвердить аккаунт <b>{user_details['username']}</b>?",
        reply_markup=reply_markup,
        parse_mode=constants.ParseMode.HTML,
    )


async def button_callback(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()
    confirmation_code = query.data
    if confirmation_code == NO:
        logger.info("User didn't want to confirm using code %s", confirmation_code)
        await query.edit_message_text(
            text="Если всё же передумаете, отправьте код доступа ещё раз."
        )
        return

    result, error_message = await confirm_user_using_code(
        confirmation_code, update.callback_query.from_user.id
    )
    if not result:
        logger.info("Cannot confirm using code %s", confirmation_code)
        await query.edit_message_text(text=error_message)
        return
    logger.info("Account successfully confirmed using code %s", confirmation_code)
    await query.edit_message_text(
        text="Ваш аккаунт подтвержден! Теперь вы сможете воспользоваться всеми "
        "возможностями нашего сайта!"
    )


if __name__ == "__main__":
    application = (
        ApplicationBuilder()
        .token(settings.bot_token)
        .connect_timeout(10)  # default 5s
        .read_timeout(30)  # default 5s
        .write_timeout(30)  # default 5s
        .get_updates_connect_timeout(60)  # default 5s
        .get_updates_pool_timeout(60)  # default 1s
        .get_updates_read_timeout(60)  # default 5s
        .get_updates_write_timeout(60)  # default 5s
        .pool_timeout(10)  # default 1s
        .build()
    )

    start_handler = CommandHandler("start", start)
    code_handler = MessageHandler(filters.TEXT & ~filters.COMMAND, code)
    application.add_handler(start_handler)
    application.add_handler(CallbackQueryHandler(button_callback))
    application.add_handler(code_handler)

    application.run_polling(
        allowed_updates=[
            Update.MESSAGE,
            Update.CALLBACK_QUERY,
        ]
    )
