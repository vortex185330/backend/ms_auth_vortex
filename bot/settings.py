import os


class Settings:
    """General settings for the bot."""

    debug = os.getenv("DEBUG", False)
    bot_token = os.getenv("BOT_TOKEN")
    auth_service_base_url = os.getenv("AUTH_SERVICE_BASE_URL")
    welcome_text = os.getenv("WELCOME_TEXT", "welcome text placeholder")
    environment = os.getenv("ENVIRONMENT", "local")
    bot_sentry_dsn = os.getenv("BOT_SENTRY_DSN", "")
    confirmation_code_internal_token_header = os.getenv(
        "CONFIRMATION_CODE_INTERNAL_TOKEN_HEADER", "X-TG-Auth"
    )
    confirmation_code_internal_token = os.getenv("CONFIRMATION_CODE_INTERNAL_TOKEN")


settings = Settings()
