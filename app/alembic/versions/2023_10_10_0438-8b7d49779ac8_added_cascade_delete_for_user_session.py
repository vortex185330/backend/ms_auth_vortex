"""Added cascade delete for user session

Revision ID: 8b7d49779ac8
Revises: 0401993f0576
Create Date: 2023-10-10 04:38:03.933208

"""
from typing import Sequence, Union

from alembic import op

# revision identifiers, used by Alembic.
revision: str = "8b7d49779ac8"
down_revision: Union[str, None] = "0401993f0576"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint("usersession_user_uuid_fkey", "usersession", type_="foreignkey")
    op.create_foreign_key(
        None, "usersession", "user", ["user_uuid"], ["uuid"], ondelete="CASCADE"
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "usersession", type_="foreignkey")
    op.create_foreign_key(
        "usersession_user_uuid_fkey", "usersession", "user", ["user_uuid"], ["uuid"]
    )
    # ### end Alembic commands ###
