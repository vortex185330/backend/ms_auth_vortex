import uuid
from datetime import datetime
from uuid import UUID

import sqlalchemy as sa
from pydantic import EmailStr
from sqlalchemy import Index, text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.core.enums import UserRole
from app.db.base_class import BaseTable


class User(BaseTable):
    """User information."""

    __table_args__ = (
        Index("idx_user_username_case_insensitive", text("LOWER(username)"), unique=True),
        Index("idx_user_email_case_insensitive", text("LOWER(email)"), unique=True),
    )

    uuid: Mapped[UUID] = mapped_column(primary_key=True, default=uuid.uuid4)
    username: Mapped[str] = mapped_column(sa.String(16), nullable=False)
    email: Mapped[EmailStr] = mapped_column(sa.String(255), nullable=False)
    telegram_id: Mapped[str] = mapped_column(
        sa.String(32), nullable=True, unique=False, index=True
    )
    password: Mapped[str] = mapped_column(sa.String(72), nullable=False)
    email_activated_at: Mapped[datetime] = mapped_column(nullable=True)
    telegram_activated_at: Mapped[datetime] = mapped_column(nullable=True)
    created_at: Mapped[datetime] = mapped_column(
        nullable=False, server_default=sa.func.now()
    )
    updated_at: Mapped[datetime] = mapped_column(
        nullable=True, server_onupdate=sa.func.now()
    )
    synced_at: Mapped[datetime] = mapped_column(nullable=True)
    deleted_at: Mapped[datetime] = mapped_column(nullable=True)
    is_active: Mapped[bool] = mapped_column(
        server_default=sa.sql.false(), nullable=False, index=True
    )
    role: Mapped[UserRole] = mapped_column(
        server_default=UserRole.user.value, nullable=False, index=True
    )
    sessions: Mapped[list["UserSession"]] = relationship(
        back_populates="user", cascade="all,delete"
    )


class UserSession(BaseTable):
    """Store user session with various details."""

    uuid: Mapped[UUID] = mapped_column(primary_key=True, default=uuid.uuid4, index=True)
    user_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey("user.uuid", ondelete="CASCADE")
    )
    user: Mapped[User] = relationship(back_populates="sessions")
    ip: Mapped[str] = mapped_column(sa.String(39), nullable=False)
    useragent: Mapped[str] = mapped_column(sa.Text(), nullable=True)
    country_iso_code: Mapped[str] = mapped_column(sa.String(3), nullable=True)
    country_name: Mapped[str] = mapped_column(sa.String(255), nullable=True)
    city_name: Mapped[str] = mapped_column(sa.String(255), nullable=True)
    longitude: Mapped[float] = mapped_column(sa.Float(10), nullable=True)
    latitude: Mapped[float] = mapped_column(sa.Float(10), nullable=True)
    last_activity: Mapped[datetime] = mapped_column(
        nullable=False, server_default=sa.func.now()
    )
    created_at: Mapped[datetime] = mapped_column(
        nullable=False, server_default=sa.func.now()
    )
