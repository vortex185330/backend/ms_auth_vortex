import logging
from pathlib import Path

import geoip2.database
from geoip2.errors import AddressNotFoundError
from maxminddb import InvalidDatabaseError

from app.schemas.geo_schema import GeoData

logger = logging.getLogger(__name__)


async def get_info_about_ip(ip_address: str) -> GeoData:
    file_path = Path(__file__).parent.absolute() / "GeoLite2-City.mmdb"
    try:
        with geoip2.database.Reader(file_path) as reader:
            response = reader.city(ip_address)
    except AddressNotFoundError:
        logger.info("Cannot fetch geo info from ip address %s", ip_address)
        return GeoData()
    except (FileNotFoundError, IsADirectoryError, InvalidDatabaseError):
        logger.error("Cannot find geo database on path %s", file_path)
        return GeoData()

    return GeoData(
        country_iso_code=response.country.iso_code,
        country_name=response.country.names.get("en"),
        city_name=response.city.names.get("en"),
        latitude=response.location.latitude,
        longitude=response.location.longitude,
    )
