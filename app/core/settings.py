from pydantic import PostgresDsn, RedisDsn
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """Settings for the application derived from environment."""

    debug: bool = False
    title: str = "Kapibara Auth Service API"
    version: str = "0.0.1-alpha"
    environment: str = "local"
    auth_postgres_user: str | None = None
    auth_postgres_password: str | None = None
    auth_postgres_server: str | None = None
    auth_postgres_db: str | None = None
    monolith_host: str | None = None
    monolith_internal_token_header: str = "X-Kapibara-Internal-Token"
    monolith_internal_token: str | None = None
    external_requests_timeout: int = 10
    jwt_algorithm: str = "RS512"
    jwt_rsa_private_key: str | None = None
    jwt_rsa_public_key: str | None = None
    jwt_access_token_lifetime_minutes: int = 5
    jwt_refresh_token_lifetime_days: int = 365
    jwt_issuer: str = "KapibaraAuth"
    jwt_audience: str = "KapibaraUsers"
    default_email_from: str = "no-reply@kapi.bar"
    smtp_username: str | None = None
    smtp_password: str | None = None
    smtp_host: str | None = None
    smtp_port: int | None = None
    redis_host: str | None = None
    redis_password: str | None = None
    redis_port: int = 6379
    redis_db: str = "1"
    celery_redis_db: str = "2"
    rate_limiter_redis_db: str = "6"
    celery_task_always_eager: bool = False
    confirmation_code_length: int = 32
    email_confirmation_code_length: int = 8
    confirmation_code_ttl: int = 15 * 60
    confirmation_code_tg_bot_url: str | None = None
    confirmation_code_internal_token_header: str = "X-TG-Auth"
    confirmation_code_internal_token: str | None = None
    # The following pattern means that after first try, the function can be called
    # only after 60 seconds. After second try - only after 5 minutes and so on.
    confirmation_code_throttling_pattern: dict[int, int] = {
        1: 1 * 10,
        2: 5 * 60,
        3: 10 * 60,
        4: 24 * 60 * 60,
    }
    auth_sentry_dsn: str | None = None
    username_min_length: int = 4
    username_max_length: int = 16
    username_allowed_chars_pattern: str = r"^[a-zA-Z0-9.\-_]+$"
    password_min_length: int = 6
    frontend_base_url: str = "https://kapi.bar"


settings = Settings()


def get_db_url() -> str:
    return str(
        PostgresDsn.build(
            scheme="postgresql",
            username=settings.auth_postgres_user,
            password=settings.auth_postgres_password,
            host=settings.auth_postgres_server,
            path=settings.auth_postgres_db,
        )
    )


def get_redis_url(db=settings.redis_db) -> str:
    return str(
        RedisDsn.build(
            scheme="redis",
            password=settings.redis_password,
            host=settings.redis_host,
            port=settings.redis_port,
            path=db,
        )
    )


def get_celery_redis_url() -> str:
    return get_redis_url(db=settings.celery_redis_db)


def get_rate_limiter_redis_url() -> str:
    return get_redis_url(db=settings.rate_limiter_redis_db)
