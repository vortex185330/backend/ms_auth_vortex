from fastapi import Request


def get_user_ip_from_request(request: Request) -> str:
    headers_to_check = ["X-Real-IP", "CF-Connecting-IP", "X-Forwarded-For", "X-Forwarded"]

    for header_to_check in headers_to_check:
        if ip_candidate := request.headers.get(header_to_check):
            try:
                ip, *_ = ip_candidate.split(",")
                return ip.strip()
            except (AttributeError, ValueError):
                pass
    return request.client.host
