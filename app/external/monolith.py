import requests
from requests import status_codes
from tenacity import (
    retry,
    retry_if_not_exception_type,
    stop_after_attempt,
    stop_after_delay,
)

from app.core.exceptions import MonolithUserSyncException
from app.core.settings import settings
from app.models.user import User
from app.schemas.user_schema import UserCreateOnMonolith


@retry(
    retry=retry_if_not_exception_type(MonolithUserSyncException),
    stop=(stop_after_delay(30) | stop_after_attempt(5)),
)
def create_user_on_monolith(user: User):
    create_user_url = f"{settings.monolith_host}/v1/users/"
    user_to_monolith = UserCreateOnMonolith(
        external_user_uid=user.uuid,
        username=user.username,
        email=user.email,
    )
    result = requests.post(
        create_user_url,
        headers={
            settings.monolith_internal_token_header: settings.monolith_internal_token
        },
        json=user_to_monolith.model_dump(mode="json"),
        timeout=settings.external_requests_timeout,
    )
    if result.status_code == status_codes.codes.BAD_REQUEST:
        raise MonolithUserSyncException(f"Error {result.content.decode()}")
    if not result.ok:
        result.raise_for_status()


@retry(
    retry=retry_if_not_exception_type(MonolithUserSyncException),
    stop=(stop_after_delay(30) | stop_after_attempt(5)),
)
def mark_user_verified_on_monolith(user: User):
    verify_user_url = f"{settings.monolith_host}/v1/users/{user.username}/verify/"
    result = requests.post(
        verify_user_url,
        headers={
            settings.monolith_internal_token_header: settings.monolith_internal_token
        },
        timeout=settings.external_requests_timeout,
    )
    if result.status_code == status_codes.codes.BAD_REQUEST:
        raise MonolithUserSyncException(f"Error {result.content.decode()}")
    if not result.ok:
        result.raise_for_status()
