import secrets
from http import HTTPStatus
from unittest.mock import patch

import pytest
from httpx import AsyncClient

from app.core.enums import ConfirmationCodeType
from app.core.settings import settings
from app.main import app
from app.models.user import User
from app.schemas.security_schema import ConfirmationCodeData


@pytest.mark.anyio
class TestGetCode:
    def setup_method(self):
        self.patch_fetch_confirmation_code_data = patch(
            "app.v1.endpoints.code.fetch_confirmation_code_data"
        )
        self.confirmation_code = secrets.token_hex(settings.confirmation_code_length // 2)

    async def test_get_code(self, user: User):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.email, user_uuid=user.uuid
            )
            result = await self._get_code(
                self.confirmation_code,
                headers={
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                },
            )
            assert result.status_code == HTTPStatus.OK
            response = result.json()
            assert response["username"] == user.username
            assert response["uuid"] == str(user.uuid)

    @pytest.mark.parametrize("header_value", ("", "wrong"))
    async def test_get_code_wrong_headers(self, header_value, user: User):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.email, user_uuid=user.uuid
            )
            result = await self._get_code(
                self.confirmation_code,
                headers={settings.confirmation_code_internal_token_header: header_value},
            )
        assert result.status_code == HTTPStatus.FORBIDDEN
        response = result.json()
        assert response["detail"][0]["type"] == "forbidden"

    async def _get_code(self, code: str, headers: dict = None):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            return await ac.get(f"/v1/code/{code}/", headers=headers or {})
