import datetime
from http import HTTPStatus
from unittest.mock import patch

import pytest
from httpx import AsyncClient
from sqlalchemy.orm import Session

from app.core.enums import UserRole
from app.main import app
from app.models.user import User


@pytest.mark.anyio
class TestActivateUserAsModer:
    def setup_method(self):
        self.patch_generate_and_email_confirmation_code = patch(
            "app.v1.endpoints.user.generate_and_email_confirmation_code"
        )
        self.patch_create_user = patch(
            "app.v1.endpoints.user.crud_user.create_user_and_sync_to_monolith"
        )

    async def test_activate(self, db: Session, access_token_and_user, user: User):
        access_token, moderator = access_token_and_user
        moderator.role = UserRole.moderator
        db.commit()
        with (
            self.patch_generate_and_email_confirmation_code as mock_email_code,
            self.patch_create_user as mock_create_user,
        ):
            mock_create_user.return_value = user

            result = await self._activate(
                username=user.username,
                headers={"Authorization": f"Bearer {access_token}"},
            )
        assert result.status_code == HTTPStatus.OK
        mock_email_code.assert_awaited_once()
        mock_create_user.assert_not_awaited()

    async def test_activate_as_not_moderator(
        self, db: Session, access_token_and_user, user: User
    ):
        access_token, _ = access_token_and_user
        with self.patch_generate_and_email_confirmation_code as mock_email_code:
            result = await self._activate(
                username=user.username,
                headers={"Authorization": f"Bearer {access_token}"},
            )
        assert result.status_code == HTTPStatus.FORBIDDEN
        mock_email_code.assert_not_awaited()

    async def test_activate_non_existing_user(self, db: Session, access_token_and_user):
        access_token, moderator = access_token_and_user
        moderator.role = UserRole.moderator
        db.commit()
        with self.patch_generate_and_email_confirmation_code as mock_email_code:
            result = await self._activate(
                username="not-found", headers={"Authorization": f"Bearer {access_token}"}
            )
        assert result.status_code == HTTPStatus.NOT_FOUND
        mock_email_code.assert_not_awaited()

    async def test_activate_already_active_user(
        self, db: Session, access_token_and_user, user: User
    ):
        access_token, moderator = access_token_and_user
        moderator.role = UserRole.moderator
        user.email_activated_at = datetime.datetime.now()
        db.commit()
        with self.patch_generate_and_email_confirmation_code as mock_email_code:
            result = await self._activate(
                username=user.username,
                headers={"Authorization": f"Bearer {access_token}"},
            )
        assert result.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
        mock_email_code.assert_not_awaited()

    async def _activate(self, username: str, headers: dict):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            return await ac.post(
                "/v1/user/activate/", headers=headers, data={"username": username}
            )
