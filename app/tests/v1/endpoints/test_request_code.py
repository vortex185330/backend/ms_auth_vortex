import datetime
import logging
import uuid
from http import HTTPStatus
from unittest import mock
from unittest.mock import patch

import jwt
import pytest
from freezegun import freeze_time
from httpx import AsyncClient
from sqlalchemy.orm import Session

from app.core.enums import ConfirmationCodeType, TokenType
from app.core.settings import settings
from app.main import app
from app.models.user import User


@pytest.mark.anyio
class TestRequestCode:
    def setup_method(self):
        self.mock_generate_confirmation_code = mock.AsyncMock(return_value="somecode")
        self.patch_generate_confirmation_code = patch(
            "app.v1.endpoints.code.generate_confirmation_code"
        )
        self.patch_get_confirmation_code_tries_lock = patch(
            "app.v1.endpoints.code.get_confirmation_code_tries_lock"
        )
        self.patch_generate_and_email_confirmation_code = patch(
            "app.v1.endpoints.code.generate_and_email_confirmation_code"
        )

    @pytest.mark.parametrize(
        "code_type,expected_code",
        (
            (ConfirmationCodeType.telegram, "somecode"),
            (ConfirmationCodeType.email, None),
        ),
    )
    async def test_request_code(self, code_type, expected_code, access_token_and_user):
        access_token, _ = access_token_and_user

        with (
            self.patch_generate_confirmation_code as mock_generate_confirmation_code,
            self.patch_get_confirmation_code_tries_lock as mock_get_code_tries_lock,
            self.patch_generate_and_email_confirmation_code as mock_email_code,
        ):
            mock_generate_confirmation_code.return_value = expected_code
            mock_get_code_tries_lock.return_value = None, None
            result = await self._request_code(
                data={"type": code_type},
                headers={"Authorization": f"Bearer {access_token}"},
            )
        if code_type == ConfirmationCodeType.email:
            mock_email_code.assert_awaited_once()
        else:
            mock_email_code.assert_not_awaited()

        assert result.status_code == HTTPStatus.CREATED
        response = result.json()
        assert response["code"] == expected_code
        assert response["type"] == code_type

    @pytest.mark.parametrize(
        "confirmed_code_type,error_message",
        (
            (
                ConfirmationCodeType.telegram,
                "Вы уже подтвердили свой аккаунт через Telegram",
            ),
            (ConfirmationCodeType.email, "Вы уже подтвердили свой аккаунт через email"),
        ),
    )
    async def test_request_code_already_confirmed(
        self,
        confirmed_code_type,
        error_message,
        db: Session,
        user: User,
        access_token_and_user,
    ):
        access_token, _ = access_token_and_user

        if confirmed_code_type == ConfirmationCodeType.email:
            user.email_activated_at = datetime.datetime.now()
        else:
            user.telegram_activated_at = datetime.datetime.now()

        db.commit()

        with (
            self.patch_generate_confirmation_code as mock_generate_confirmation_code,
            self.patch_get_confirmation_code_tries_lock as mock_get_code_tries_lock,
            self.patch_generate_and_email_confirmation_code as mock_email_code,
        ):
            result = await self._request_code(
                data={"type": confirmed_code_type},
                headers={"Authorization": f"Bearer {access_token}"},
            )
        assert result.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

        mock_generate_confirmation_code.assert_not_awaited()
        mock_get_code_tries_lock.assert_not_awaited()
        mock_email_code.assert_not_awaited()

        response = result.json()
        assert response["detail"][0]["type"] == "account_already_confirmed"
        assert response["detail"][0]["msg"] == error_message

    @pytest.mark.parametrize(
        "num_tries,log_level",
        (
            (1, logging.WARNING),
            (2, logging.WARNING),
            (3, logging.ERROR),
            (4, logging.ERROR),
        ),
    )
    async def test_request_code_too_many_requests(
        self, num_tries, log_level, access_token_and_user, caplog
    ):
        access_token, user = access_token_and_user
        seconds_locked_for = 300
        with (
            self.patch_get_confirmation_code_tries_lock as mock_get_code_tries_lock,
            self.patch_generate_and_email_confirmation_code as mock_email_code,
        ):
            mock_get_code_tries_lock.return_value = num_tries, seconds_locked_for

            result = await self._request_code(
                data={"type": ConfirmationCodeType.email},
                headers={"Authorization": f"Bearer {access_token}"},
            )

        assert result.status_code == HTTPStatus.TOO_MANY_REQUESTS
        mock_email_code.assert_not_awaited()

        response = result.json()
        assert response["detail"][0]["type"] == "too_many_requests"
        assert response["detail"][0]["msg"] == (
            f"Вы запрашиваете код подтверждения слишком часто. "
            f"Попробуйте через {seconds_locked_for // 60} минут"
        )

        # this error should be the first entry in caplog, but better to search by path
        log_entry = list(
            filter(lambda r: r.name.endswith("endpoints.code"), caplog.records)
        )
        assert len(log_entry) > 0, caplog.records
        log_entry = log_entry[0]
        assert log_entry.levelno == log_level
        assert log_entry.message == (
            f"User {user.username} requesting email confirmation code for {num_tries} "
            f"time, locked for {seconds_locked_for} seconds"
        )

    async def test_request_code_wrong_code_type(self, access_token_and_user):
        access_token, _ = access_token_and_user

        result = await self._request_code(
            data={"type": "kapibara"},
            headers={"Authorization": f"Bearer {access_token}"},
        )

        assert result.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    async def test_request_code_wrong_token(self, refresh_token):
        result = await self._request_code(
            headers={"Authorization": f"Bearer {refresh_token}"}
        )
        assert result.status_code == HTTPStatus.BAD_REQUEST
        response = result.json()
        assert response["detail"][0]["type"] == "wrong_token_type"

    async def test_request_code_expired_token(self, access_token_and_user):
        access_token, _ = access_token_and_user
        with freeze_time(
            datetime.datetime.now()
            + datetime.timedelta(minutes=settings.jwt_access_token_lifetime_minutes + 1)
        ):
            result = await self._request_code(
                headers={"Authorization": f"Bearer {access_token}"}
            )
        assert result.status_code == HTTPStatus.UNAUTHORIZED
        response = result.json()
        assert response["detail"][0]["type"] == "token_expired"

    async def test_request_code_without_bearer(self):
        result = await self._request_code(headers={})
        assert result.status_code == HTTPStatus.FORBIDDEN
        response = result.json()
        assert response["detail"][0]["type"] == "forbidden"

    @pytest.mark.parametrize("jti", (None, "wrong", uuid.uuid4()))
    @pytest.mark.parametrize("user_id", (None, "random", uuid.uuid4()))
    async def test_request_code_without_user_in_db(self, db, user_id, jti):
        token = dict(
            exp=datetime.datetime.now()
            + datetime.timedelta(days=settings.jwt_refresh_token_lifetime_days),
            iss=settings.jwt_issuer,
            aud=settings.jwt_audience,
            token_type=TokenType.access,
        )
        if user_id:
            token["user_id"] = str(user_id)
        if jti:
            token["jti"] = str(jti)

        access_token = jwt.encode(
            payload=token,
            key=settings.jwt_rsa_private_key,
            algorithm=settings.jwt_algorithm,
        )
        result = await self._request_code(
            headers={"Authorization": f"Bearer {access_token}"}
        )
        assert result.status_code == HTTPStatus.UNAUTHORIZED
        response = result.json()
        assert response["detail"][0]["type"] in ("token_invalid", "user_not_found")

    @pytest.mark.parametrize(
        "auth_header,expected_error_type,expected_status_code",
        (
            ("Bearer test", "token_invalid", HTTPStatus.UNAUTHORIZED),
            ("just content", "forbidden", HTTPStatus.FORBIDDEN),
        ),
    )
    async def test_request_code_wrong_auth_header(
        self, auth_header, expected_error_type, expected_status_code
    ):
        result = await self._request_code(headers={"Authorization": auth_header})
        assert result.status_code == expected_status_code
        response = result.json()
        assert response["detail"][0]["type"] == expected_error_type

    async def _request_code(self, headers: dict, data: dict = None):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            return await ac.post("/v1/code/", json=data, headers=headers)
