import datetime
import secrets
import uuid
from http import HTTPStatus
from unittest.mock import patch

import pytest
from httpx import AsyncClient
from sqlalchemy.orm import Session

from app.core.enums import ConfirmationCodeType
from app.core.settings import settings
from app.core.utils.security import decode_token
from app.main import app
from app.models.user import User
from app.schemas.security_schema import ConfirmationCodeData


@pytest.mark.anyio
class TestConfirmCode:
    def setup_method(self):
        self.patch_fetch_confirmation_code_data = patch(
            "app.v1.endpoints.code.fetch_confirmation_code_data"
        )
        self.confirmation_code = secrets.token_hex(settings.confirmation_code_length // 2)
        self.email_confirmation_code = "12345678"

    @patch("app.crud.crud_user.create_user_and_sync_to_monolith")
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_confirm_code(
        self,
        mock_mark_user_verified_on_monolith,
        mock_create_user_and_sync_to_monolith,
        user: User,
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.email, user_uuid=user.uuid
            )
            result = await self._confirm_code({"code": self.email_confirmation_code})
            assert result.status_code == HTTPStatus.OK
            assert user.is_active is False
            assert user.email_activated_at is not None
            assert user.telegram_activated_at is None
            mock_create_user_and_sync_to_monolith.assert_called_once()
            mock_mark_user_verified_on_monolith.assert_not_called()
            response = result.json()
            access_token = await decode_token(response["access_token"])
            assert access_token.is_active is False
            assert access_token.is_email_activated is True
            assert access_token.is_telegram_activated is False

            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            result = await self._confirm_code(
                {"code": self.confirmation_code, "telegram_id": "1234"},
                headers={
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                },
            )
            assert result.status_code == HTTPStatus.OK
            assert user.is_active is True
            assert user.email_activated_at is not None
            assert user.telegram_activated_at is not None
            mock_mark_user_verified_on_monolith.assert_called_once()
            response = result.json()
            assert response is None

    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_email_code_cant_be_confirm_via_telegram(
        self, mock_mark_user_verified_on_monolith, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.email, user_uuid=user.uuid
            )
            result = await self._confirm_code(
                {"code": self.email_confirmation_code},
                headers={
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                },
            )
            assert result.status_code == HTTPStatus.NOT_FOUND
            assert user.is_active is False
            assert user.email_activated_at is None
            assert user.telegram_activated_at is None
            mock_mark_user_verified_on_monolith.assert_not_called()
            response = result.json()
            assert response["detail"][0]["type"] == "not_found"

    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_telegram_code_cant_be_confirm_via_email(
        self, mock_mark_user_verified_on_monolith, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            result = await self._confirm_code({"code": self.confirmation_code})
            assert result.status_code == HTTPStatus.FORBIDDEN
            assert user.is_active is False
            assert user.email_activated_at is None
            assert user.telegram_activated_at is None
            mock_mark_user_verified_on_monolith.assert_not_called()
            response = result.json()
            assert response["detail"][0]["type"] == "forbidden"

    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_cant_confirm_another_account_with_same_telegram_id(
        self, mock_mark_user_verified_on_monolith, db: Session, user: User
    ):
        telegram_id = "1234"
        db_user = User(
            username="old user",
            email="kapiold@kapi.bar",
            password="password",
            telegram_id=telegram_id,
        )
        db.add(db_user)
        db.commit()
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            headers = {
                settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
            }
            result = await self._confirm_code(
                {
                    "code": self.confirmation_code,
                    "telegram_id": telegram_id,
                },
                headers=headers,
            )
        assert result.status_code == HTTPStatus.CONFLICT
        mock_mark_user_verified_on_monolith.assert_not_called()
        response = result.json()
        assert response["detail"][0]["type"] == "duplicate_telegram_id"
        assert (
            response["detail"][0]["msg"] == "Аккаунт с таким Телеграм уже зарегистрирован"
        )

    @pytest.mark.parametrize(
        "code_type", (ConfirmationCodeType.email, ConfirmationCodeType.telegram)
    )
    @patch("app.crud.crud_user.create_user_and_sync_to_monolith")
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_confirm_individual_code(
        self,
        mock_mark_user_verified_on_monolith,
        mock_create_user_and_sync_to_monolith,
        code_type,
        user: User,
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=code_type, user_uuid=user.uuid
            )
            if code_type == ConfirmationCodeType.telegram:
                headers = {
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                }
            else:
                headers = {}
            result = await self._confirm_code(
                {"code": self.confirmation_code, "telegram_id": "1234"},
                headers=headers,
            )
            assert result.status_code == HTTPStatus.OK
            assert user.is_active is False
            if code_type == ConfirmationCodeType.email:
                assert user.email_activated_at is not None
                assert user.telegram_activated_at is None
                mock_create_user_and_sync_to_monolith.assert_called_once()
            if code_type == ConfirmationCodeType.telegram:
                assert user.email_activated_at is None
                assert user.telegram_activated_at is not None
                mock_create_user_and_sync_to_monolith.assert_not_called()
            mock_mark_user_verified_on_monolith.assert_not_called()

    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_telegram_code_cant_be_confirmed_without_headers(
        self, mock_mark_user_verified_on_monolith, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            result = await self._confirm_code({"code": self.confirmation_code})
        assert result.status_code == HTTPStatus.FORBIDDEN
        assert user.telegram_activated_at is None
        mock_mark_user_verified_on_monolith.assert_not_called()
        response = result.json()
        assert response["detail"][0]["type"] == "forbidden"

    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_telegram_code_cant_be_confirmed_without_telegram_id(
        self, mock_mark_user_verified_on_monolith, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            headers = {
                settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
            }
            result = await self._confirm_code(
                {"code": self.confirmation_code}, headers=headers
            )
        assert result.status_code == HTTPStatus.FORBIDDEN
        assert user.telegram_activated_at is None
        mock_mark_user_verified_on_monolith.assert_not_called()
        response = result.json()
        assert response["detail"][0]["type"] == "forbidden"

    @pytest.mark.parametrize("header_value", ("", "wrong"))
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_telegram_code_cant_be_confirmed_with_wrong_headers(
        self, mock_mark_user_verified_on_monolith, header_value, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            result = await self._confirm_code(
                {"code": self.confirmation_code, "telegram_id": "1234"},
                headers={settings.confirmation_code_internal_token_header: header_value},
            )
        assert result.status_code == HTTPStatus.FORBIDDEN
        assert user.telegram_activated_at is None
        mock_mark_user_verified_on_monolith.assert_not_called()
        response = result.json()
        assert response["detail"][0]["type"] == "forbidden"

    @pytest.mark.parametrize(
        "code_type", (ConfirmationCodeType.email, ConfirmationCodeType.telegram)
    )
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_confirm_with_wrong_code(
        self, mock_mark_user_verified_on_monolith, code_type, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=code_type, user_uuid=user.uuid
            )
            result = await self._confirm_code({"code": "wrong"})
            assert result.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
            assert user.is_active is False
            assert user.email_activated_at is None
            assert user.telegram_activated_at is None
            mock_mark_user_verified_on_monolith.assert_not_called()
            response = result.json()
            assert response["detail"][0]["type"] == "string_too_short"

    @pytest.mark.parametrize(
        "code_type", (ConfirmationCodeType.email, ConfirmationCodeType.telegram)
    )
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_confirm_not_found_code(
        self, mock_mark_user_verified_on_monolith, code_type, user: User
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = None
            result = await self._confirm_code({"code": self.confirmation_code})
            assert result.status_code == HTTPStatus.NOT_FOUND
            assert user.is_active is False
            assert user.email_activated_at is None
            assert user.telegram_activated_at is None
            mock_mark_user_verified_on_monolith.assert_not_called()
            response = result.json()
            assert response["detail"][0]["type"] == "not_found"

    async def test_confirm_code_monolith_not_responding(self, db: Session, user: User):
        user.email_activated_at = datetime.datetime.now()
        db.commit()

        with patch(
            "app.crud.crud_user.mark_user_verified_on_monolith",
            side_effect=Exception("test"),
        ), self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=ConfirmationCodeType.telegram, user_uuid=user.uuid
            )
            result = await self._confirm_code(
                {"code": self.confirmation_code, "telegram_id": "1234"},
                headers={
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                },
            )

            assert result.status_code == HTTPStatus.SERVICE_UNAVAILABLE
            response = result.json()
            assert response["detail"][0]["type"] == "cant_sync_user_to_monolith"

    @pytest.mark.parametrize(
        "code_type", (ConfirmationCodeType.email, ConfirmationCodeType.telegram)
    )
    @patch("app.crud.crud_user.mark_user_verified_on_monolith")
    async def test_confirm_with_non_existing_user(
        self, mock_mark_user_verified_on_monolith, code_type, db: Session
    ):
        with self.patch_fetch_confirmation_code_data as mock_fetch_confirmation_code_data:
            mock_fetch_confirmation_code_data.return_value = ConfirmationCodeData(
                code_type=code_type, user_uuid=uuid.uuid4()
            )
            result = await self._confirm_code(
                {"code": self.confirmation_code, "telegram_id": "1234"},
                headers={
                    settings.confirmation_code_internal_token_header: settings.confirmation_code_internal_token  # noqa: E501
                },
            )
            assert result.status_code == HTTPStatus.NOT_FOUND
            mock_mark_user_verified_on_monolith.assert_not_called()

    async def _confirm_code(self, data: dict = None, headers: dict = None):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            return await ac.post("/v1/code/confirm/", json=data, headers=headers or {})
