import logging
from http import HTTPStatus
from typing import Annotated

from fastapi import APIRouter, Body, Depends, Path, Request
from redis.asyncio import Redis
from sqlalchemy.orm import Session

from app import deps
from app.core.enums import ConfirmationCodeType
from app.core.exceptions import (
    AccountAlreadyConfirmed,
    DuplicateTelegram,
    Forbidden,
    NotFound,
    TooManyConfirmationCodeRequests,
)
from app.core.settings import settings
from app.core.utils.security import (
    KapibaraRateLimiter,
    fetch_confirmation_code_data,
    generate_and_email_confirmation_code,
    generate_confirmation_code,
    generate_jwt_access_token,
    generate_jwt_refresh_token,
    get_confirmation_code_tries_lock,
)
from app.crud import crud_user, crud_user_session
from app.crud.crud_user import update_user_activation_status_and_sync_to_monolith
from app.custom_types.request_types import TelegramHeader, UserAgent
from app.models.user import User
from app.schemas import user_schema
from app.schemas.response_schema import HTTPResponse
from app.schemas.security_schema import ConfirmationCodeOut

router = APIRouter()
logger = logging.getLogger(__name__)


@router.get(
    "/{code}/",
    response_model=user_schema.UserMinimalOut,
    status_code=HTTPStatus.OK,
    summary="Данные пользователя по коду подтверждения",
)
async def get_code(
    code: str = Path(
        max_length=settings.confirmation_code_length,
        min_length=settings.confirmation_code_length,
        pattern="^[a-f0-9]+$",
    ),
    db: Session = Depends(deps.get_db),
    redis: Redis = Depends(deps.get_redis),
    telegram_header: TelegramHeader = None,
):
    if (
        telegram_header != settings.confirmation_code_internal_token
        or not telegram_header
    ):
        raise Forbidden()
    code_data = await fetch_confirmation_code_data(redis, code)
    if not code_data:
        logger.info("Code %s cannot be found in redis", code)
        raise NotFound("Код активации не найден.")

    user = await crud_user.get_by_uuid(db, code_data.user_uuid)
    if not user:
        logger.info("User cannot be found by using code %s", code)
        raise NotFound("Пользователь не найден")

    return user


@router.post(
    "/confirm/",
    response_model=user_schema.UserWithJWT | None,
    status_code=HTTPStatus.OK,
    summary="Подтверждение аккаунта",
    responses={
        HTTPStatus.UNPROCESSABLE_ENTITY: {
            "model": HTTPResponse,
            "description": "Неверный формат кода подтверждения",
        },
        HTTPStatus.NOT_FOUND: {
            "model": HTTPResponse,
            "description": "Не найден код либо пользователь",
        },
        HTTPStatus.SERVICE_UNAVAILABLE: {
            "model": HTTPResponse,
            "description": "Ошибка синхронизации пользователя с монолитом. "
            "Необходимо повторить отправку данных.",
        },
    },
    dependencies=[Depends(KapibaraRateLimiter(times=1, seconds=3))],
)
async def confirm_code(  # noqa: CCR001
    request: Request,
    code: str = Body(
        embed=True,
        max_length=settings.confirmation_code_length,
        min_length=settings.email_confirmation_code_length,
        pattern="^[a-f0-9]+$",
    ),
    telegram_id: str = Body(embed=True, default=None),
    db: Session = Depends(deps.get_db),
    redis: Redis = Depends(deps.get_redis),
    user_agent: UserAgent = None,
    telegram_header: TelegramHeader = None,
):
    """Подтверждает пользовательский аккаунт.

    Этот API может вызываться как клиентом, так и телеграм ботом. В первом случае будет
    сгенерирована пользовательская сессия и токены. В случае активации через телеграм-бота
    ответ будет пустым.

    В случае активации через телеграм, необходимо дополнительно передать secret token в
    header.
    """
    code_data = await fetch_confirmation_code_data(redis, code)
    if not code_data:
        logger.info("Code %s cannot be found in redis", code)
        raise NotFound("Код активации не найден.")

    if code_data.code_type == ConfirmationCodeType.telegram and (
        telegram_header != settings.confirmation_code_internal_token
        or not telegram_header
    ):
        raise Forbidden()

    if code_data.code_type == ConfirmationCodeType.email and telegram_header:
        logger.info(
            "User seems trying to send email confirmation code %s via Telegram", code
        )
        raise NotFound("Код активации не найден.")

    if code_data.code_type == ConfirmationCodeType.telegram:
        if not telegram_id:
            raise Forbidden()
        telegram_exists_already = await crud_user.get_by_telegram_id(db, telegram_id)
        if telegram_exists_already.count() > 0:
            raise DuplicateTelegram()
    user = await update_user_activation_status_and_sync_to_monolith(
        db, code_data, telegram_id
    )

    if code_data.code_type == ConfirmationCodeType.telegram:
        # Telegram activation code will be coming from our telegram bot, so no need to
        # create user session and send back tokens
        return
    user_session = await crud_user_session.create_user_session(
        db=db, user=user, request=request, user_agent=user_agent
    )
    return user_schema.UserWithJWT(
        uuid=user.uuid,
        username=user.username,
        email=user.email,
        access_token=await generate_jwt_access_token(user, jti=user_session.uuid),
        refresh_token=await generate_jwt_refresh_token(user=user, jti=user_session.uuid),
    )


@router.post(
    "/",
    summary="Запрос кода активации",
    status_code=HTTPStatus.CREATED,
    response_model=ConfirmationCodeOut,
    response_description="Код активации сгенерирован",
    responses={
        HTTPStatus.UNAUTHORIZED: {
            "model": HTTPResponse,
            "description": "Срок действия токена вышел, необходима повторная "
            "авторизация.",
        },
        HTTPStatus.BAD_REQUEST: {
            "model": HTTPResponse,
            "description": "Неверный тип токена, использован `refresh_token` "
            "вместо `access_token`",
        },
        HTTPStatus.FORBIDDEN: {
            "model": HTTPResponse,
            "description": "Отсутствует заголовок авторизации",
        },
        HTTPStatus.NOT_FOUND: {
            "model": HTTPResponse,
            "description": "Сессия не найдена",
        },
        HTTPStatus.UNPROCESSABLE_ENTITY: {
            "model": HTTPResponse,
            "description": "Аккаунт уже был подтвержден",
        },
        HTTPStatus.TOO_MANY_REQUESTS: {
            "model": HTTPResponse,
            "description": "Слишком частые запросы на генерацию кода подтверждения",
        },
        HTTPStatus.INTERNAL_SERVER_ERROR: {
            "model": HTTPResponse,
            "description": "Ошибка сервера",
        },
    },
)
async def request_code(
    type_: Annotated[
        ConfirmationCodeType,
        Body(
            embed=True,
            alias="type",
            validation_alias="type",
        ),
    ],
    current_user_and_session_uuid: tuple[User, str] = Depends(
        deps.get_current_user_and_session_uuid
    ),
    redis: Redis = Depends(deps.get_redis),
):
    """Генерирует код активации аккаунта.

    Если параметр `type` = `email`, код не будет содержаться в ответе API, а будет
    отправлен пользователю на email.

    В случае если параметр `type` = `telegram`, код будет в ответе API и пользователю
    нужно будет его отправить нашему телеграм-боту.

    Необходима авторизация по токену, который должен быть передан в headers.
    ```
    Authorization: Bearer <access_token>
    """
    user, _ = current_user_and_session_uuid

    if type_ == ConfirmationCodeType.telegram:
        if user.telegram_activated_at is not None:
            raise AccountAlreadyConfirmed(
                "Вы уже подтвердили свой аккаунт через Telegram"
            )
        code = await generate_confirmation_code(redis, user, type_, throttle=False)
        if tg_bot_url := settings.confirmation_code_tg_bot_url:
            tg_bot_url = f"{tg_bot_url}?start={code}"

        return ConfirmationCodeOut(type=type_, code=code, url=tg_bot_url)

    if user.email_activated_at is not None:
        raise AccountAlreadyConfirmed("Вы уже подтвердили свой аккаунт через email")

    locked_try_number, seconds_locked_for = await get_confirmation_code_tries_lock(
        redis, user, type_
    )
    if locked_try_number:
        logger.log(
            logging.WARNING if locked_try_number <= 2 else logging.ERROR,
            "User %s requesting email confirmation code for %s time, "
            "locked for %s seconds",
            user.username,
            locked_try_number,
            seconds_locked_for,
        )
        minutes_locked_for = seconds_locked_for // 60
        raise TooManyConfirmationCodeRequests(
            f"Вы запрашиваете код подтверждения слишком часто. "
            f"Попробуйте через {minutes_locked_for} минут"
        )

    await generate_and_email_confirmation_code(redis, user, throttle=True)

    return ConfirmationCodeOut(type=type_)
