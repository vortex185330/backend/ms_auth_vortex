from celery.schedules import crontab

from app.core.settings import get_celery_redis_url, settings

broker_url = get_celery_redis_url()
result_backend = get_celery_redis_url()
imports = ("app.tasks",)
task_always_eager = settings.celery_task_always_eager
task_time_limit = 600
task_acks_late = True
beat_schedule = {
    "delete_non_active_users": {
        "task": "app.tasks.delete_non_active_users_task",
        "schedule": crontab(minute="*/5"),
    }
}
