from celery import shared_task

from app.crud.crud_user import delete_non_active_users


@shared_task
def delete_non_active_users_task(created_minutes_ago: int = 60):
    delete_non_active_users(created_minutes_ago)
