import uuid
from typing import Optional

from pydantic import BaseModel, Field, constr, model_validator
from pydantic_core import PydanticCustomError
from pydantic_core.core_schema import ValidationInfo

from app.core.enums import ConfirmationCodeType


class ConfirmationCodeData(BaseModel):
    """Details about confirmation code."""

    user_uuid: uuid.UUID
    code_type: ConfirmationCodeType
    telegram_id: str = ""


class RestorePasswordData(BaseModel):
    """Data to be used to restore password."""

    code: str
    password: str


class ResetPasswordData(BaseModel):
    """Data to be used to reset password."""

    username: Optional[constr(strip_whitespace=True)] = None
    email: Optional[constr(strip_whitespace=True)] = None

    @model_validator(mode="after")
    @classmethod
    def check_username_or_email(cls, value, info: ValidationInfo):
        if not value.email and not value.username:
            raise PydanticCustomError(
                "missing_field",
                "Для воcстановления пароля, требуется ввести username или пароль.",
            )
        elif value.email and value.username:
            value.username = None
        return value


class ConfirmationCodeOut(BaseModel):
    """Response model for confirmation code."""

    type_: ConfirmationCodeType = Field(alias="type", validation_alias="type")
    code: str | None = None
    url: str | None = None
