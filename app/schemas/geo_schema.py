from pydantic import BaseModel


class GeoData(BaseModel):
    """Geolocation data coming from geocoder."""

    country_iso_code: str | None = None
    country_name: str | None = None
    city_name: str | None = None
    latitude: float | None = None
    longitude: float | None = None
