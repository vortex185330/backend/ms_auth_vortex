import datetime
import logging
import uuid

from pydantic import EmailStr
from sqlalchemy import func
from sqlalchemy.orm import Session
from tenacity import RetryError

from app.core.enums import ConfirmationCodeType
from app.core.exceptions import (
    MonolithUserSyncException,
    NotFound,
    UserExternalCreationError,
)
from app.core.utils.security import generate_hashed_password
from app.external.monolith import create_user_on_monolith, mark_user_verified_on_monolith
from app.models.user import User
from app.schemas import user_schema
from app.schemas.security_schema import ConfirmationCodeData

logger = logging.getLogger(__name__)


async def update_user_password(
    db: Session, db_user: User, obj_in: user_schema.UserPasswordUpdate
) -> User | None:
    """
    Will update User in DB. Mostly reserved for the password resetting.
    """
    db_user.password = generate_hashed_password(obj_in.password)
    db.commit()
    return db_user


async def create_user(db: Session, user: user_schema.UserCreate) -> User:
    db_user = User(**user.model_dump())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


async def create_user_and_sync_to_monolith(
    db: Session, user: user_schema.UserCreate, should_sync_to_monolith: bool = True
) -> User:
    db_user = await get_by_username(db, user.username)
    if not db_user:
        db_user = User(
            uuid=uuid.uuid4(),
            username=user.username,
            email=user.email,
            password=generate_hashed_password(user.password),
        )
        db.add(db_user)
    if should_sync_to_monolith:
        try:
            create_user_on_monolith(user=db_user)
            db_user.synced_at = datetime.datetime.now()
        except Exception as e:
            db.rollback()
            logger.exception("Can't create user %s on monolith", user.username)
            raise MonolithUserSyncException(f"Error {e}")
        logger.info("User %s synced to the monolith", user.username)
    else:
        logger.info(
            "Syncing to monolith disabled, need to do it manually for user %s",
            user.username,
        )
    db.commit()
    return db_user


async def update_user_activation_status_and_sync_to_monolith(  # noqa: CCR001
    db: Session, code_data: ConfirmationCodeData, telegram_id: str | None = None
) -> User:
    db_user = await get_by_uuid(db, code_data.user_uuid)
    if not db_user:
        raise NotFound("Пользователь не найден")

    if (
        code_data.code_type == ConfirmationCodeType.telegram
        and not db_user.telegram_activated_at
    ):
        logger.info("User %s has confirmed their telegram account", db_user.username)
        db_user.telegram_activated_at = datetime.datetime.now()
        db_user.telegram_id = telegram_id
        db.commit()

    if (
        code_data.code_type == ConfirmationCodeType.email
        and not db_user.email_activated_at
    ):
        try:
            logger.info("Syncing user %s to monolith", db_user.username)
            await create_user_and_sync_to_monolith(
                db=db,
                user=user_schema.UserCreate(
                    username=db_user.username,
                    email=db_user.email,
                    password=db_user.password,
                ),
            )
        except (MonolithUserSyncException, RetryError):
            raise UserExternalCreationError()

        logger.info("User %s has confirmed their email account", db_user.username)
        db_user.email_activated_at = datetime.datetime.now()
        db.commit()

    if db_user.telegram_activated_at and db_user.email_activated_at:
        logger.info(
            "User %s has confirmed their email and telegram accounts", db_user.username
        )
        db_user.is_active = True
        try:
            logger.info("Mark user %s verified on monolith", db_user.username)
            mark_user_verified_on_monolith(db_user)
            db_user.synced_at = datetime.datetime.now()
        except Exception as e:
            db.rollback()
            # logger.exception("Can't create user %s on monolith", db_user.username)
            raise MonolithUserSyncException(f"Error {e}")

        logger.info("User %s verified on the monolith", db_user.username)
        db.commit()

    return db_user


async def get_by_email(db: Session, email: EmailStr) -> User | None:
    return db.query(User).filter(func.lower(User.email) == email.lower()).first()


async def get_by_username(db: Session, username: str) -> User | None:
    return db.query(User).filter(func.lower(User.username) == username.lower()).first()


async def get_by_uuid(db: Session, user_uuid: str | uuid.UUID) -> User | None:
    return db.query(User).filter(User.uuid == user_uuid).first()


async def get_by_telegram_id(db: Session, telegram_id: str):
    return db.query(User).filter(User.telegram_id == telegram_id)


def delete_non_active_users(created_minutes_ago: int = 60):
    from app.managers import db_manager

    time_ago = datetime.datetime.now() - datetime.timedelta(minutes=created_minutes_ago)
    logger.info("Deleting non activated user accounts created before %s", time_ago)
    with db_manager() as db:
        query = db.query(User).filter(
            # Удаляем только тех, кто еще не активировал емаил
            User.email_activated_at.is_(None),
            # и только тех, кто не засинкался с монолитом
            User.synced_at.is_(None),
            # аккаунты созданные до time_ago
            User.created_at < time_ago,
        )
        usernames = [user.username for user in query]
        logger.info("Deleting %s usernames %s", query.count(), ", ".join(usernames))

        query.delete()
        db.commit()
