import datetime
import uuid
from typing import Type

from fastapi import Request
from sqlalchemy.orm import Session

from app.core.geo.helpers import get_info_about_ip
from app.core.utils.request import get_user_ip_from_request
from app.models.user import User, UserSession


async def create_user_session(
    *, db: Session, user: User, request: Request, user_agent: str | None = None
) -> UserSession:
    ip = get_user_ip_from_request(request)
    geo_data = await get_info_about_ip(ip)
    user_session = UserSession(
        user=user,
        ip=ip,
        useragent=user_agent,
        country_iso_code=geo_data.country_iso_code,
        country_name=geo_data.country_name,
        city_name=geo_data.city_name,
        longitude=geo_data.longitude,
        latitude=geo_data.latitude,
    )
    db.add(user_session)
    db.commit()
    return user_session


async def refresh_user_session(
    *,
    db: Session,
    user_session: UserSession,
    request: Request | None = None,
    user_agent: str | None = None,
):
    if request:
        ip = get_user_ip_from_request(request)
        geo_data = await get_info_about_ip(ip)
        user_session.ip = ip
        user_session.country_iso_code = geo_data.country_iso_code
        user_session.country_name = geo_data.country_name
        user_session.city_name = geo_data.city_name
        user_session.longitude = geo_data.longitude
        user_session.latitude = geo_data.latitude
    if user_agent:
        user_session.useragent = user_agent
    user_session.last_activity = datetime.datetime.now()
    db.commit()


async def delete_user_sessions(
    *, db: Session, user: User, exclude_uuids: list[uuid.UUID | str] | None = None
) -> None:
    query = db.query(UserSession).filter(UserSession.user == user)
    if exclude_uuids:
        query = query.filter(UserSession.uuid.notin_(exclude_uuids))
    query.delete()
    db.commit()


async def delete_user_session(
    *, db: Session, user: User, user_session_uuid: str | uuid.UUID
) -> bool:
    query = db.query(UserSession).filter(
        UserSession.user == user, UserSession.uuid == user_session_uuid
    )
    result = query.delete()
    db.commit()
    return bool(result)


async def get_user_sessions_by_user_uuid(
    db: Session, user_uuid: str | uuid.UUID
) -> list[Type[UserSession]] | None:
    return db.query(UserSession).filter(UserSession.user_uuid == user_uuid).all()


async def get_user_session_by_uuid(
    db: Session, user_session_uuid: str | uuid.UUID
) -> UserSession | None:
    return db.query(UserSession).filter(UserSession.uuid == user_session_uuid).first()
