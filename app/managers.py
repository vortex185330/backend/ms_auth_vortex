from contextlib import contextmanager

from app.deps import get_db

db_manager = contextmanager(get_db)
