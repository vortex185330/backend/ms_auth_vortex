import logging
from contextlib import asynccontextmanager
from http import HTTPStatus

from celery import Celery
from fastapi import Depends, FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from pydantic import ValidationError
from redis.asyncio import Redis
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from sqlalchemy import text
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from app import deps
from app.core.exceptions import KapibaraException
from app.core.settings import settings
from app.core.utils.security import init_limiter
from app.v1.urls import router

logging.basicConfig(
    level=logging.DEBUG if settings.debug else logging.INFO,
    format="%(levelname)s %(asctime)s %(name)s %(message)s",
)

api_description = f"""

Данные API предназначены для регистрации пользователя, подтверждения аккаунта, управления
пользовательскими сессиями, а так же выдачу JWT токенов и обновление JWT access токена.

Типичный сценарий использования для регистрации нового пользователя таков:

### POST /v1/user/register/ - Регистрация

`POST /v1/user/register/` для регистрации аккаунта. В теле запроса необходимо отправить
`username`, `email` и `password`.

После валидации будет создан неверифицированный пользовательский аккаунт на этом сервисе,
а так же на основном монолите.

В ответ придет пара JWT access и refresh токенов, в которых будет содержаться информация
о пользовательском идентификаторе, а так же статусах подтверждения.

Пользователю будет отправлен email c ссылкой, содержащей код активации. С этого момента
пользователь сможет авторизоваться через `POST /v1/user/login/`.

JWT access токен необходимо использовать в заголовках запросов
```
Authentication: Bearer <token>
```

### POST /v1/code/ - Код активации (*Необходима авторизация*)


`POST /v1/code/` служит для генерации кода активации. В теле запроса необходимо отправить
тип кода, который мы хотим получить. Это может быть `email` или `telegram`.

В случае `email` будет сформирован email и отправлен пользователю, в случае `telegram` код
будет возвращен клиенту, а так же URL телеграм бота, через который можно подтвердить
аккаунт.

Время жизни кодов активации - {settings.confirmation_code_ttl // 60} минут.
В случае если аккаунт не подтвержден в течении этого времени, необходимо сгенерировать код
еще раз.

**Внимание!** Для email кода активации существуют ограничения по частоте его
генерации. (Первое число - попытка, второе - число секунд до следующей попытки)

{[
    f'Попытка: {tries} - Секунд: {seconds}'
    for tries, seconds in settings.confirmation_code_throttling_pattern.items()
]}


### POST /v1/code/confirm/ - Активация аккаунта

`POST /v1/code/confirm/` - подтверждает пользовательский аккаунт. Используется
либо клиентом, либо телеграм ботом. Для подтверждения необходимо отправить код в теле
запроса, в ответ будут возвращены данные пользователя а так же JWT access / refresh токены


### POST /v1/user/login/ - Логин пользователя

Для логина пользователя через `POST /v1/user/login/` необходимо отправить в теле запроса
`username` и `password`, при успешном логине будет сгенерирована пара JWT токенов, а так
же создана сессия пользователя, которую можно получить через `/v1/session/`

### POST /v1/token/refresh/ - Обновление токена

Если API стали выдавать ошибку авторизации, необходимо обновить access token. Для этого
необходимо отправить запрос `POST /v1/token/refresh/`, содержащий в теле `refresh_token` и
в ответ придет пара JWT access / refresh токенов

Время жизни токенов

* access_token - {settings.jwt_access_token_lifetime_minutes} минут
* refresh_token - {settings.jwt_refresh_token_lifetime_days} дней

### Управление сессиями (*Необходима авторизация*)

* `GET /v1/session/` - список всех сессий пользователя, содержащий User agent, IP и время
создания
* `DELETE /v1/session/` - удаление всех сессий, A.K.A. разлогинивание пользователя со всех
устройств, где данный аккаунт был использован
* `DELETE /v1/session/<uuid>/` - удаление одной сессии

### POST /v1/password/reset/ - Cброс пароля

`POST /v1/password/reset/` принимает в теле запроса `email` или `username` и если
пользователь найден - отправляет на емаил инструкцию по восстановлению пароля, которая
содержит ссылку с кодом.

Код необходимо использовать при установке нового пароля.

### POST /v1/password/confirm/ - Установка нового пароля

Для того, чтобы установить новый пароль, необходимо отправить в теле запроса новый пароль
и код, полученный после сброса пароля через `POST /v1/password/reset/`

"""


@asynccontextmanager
async def lifespan(app: FastAPI):
    await init_limiter()
    yield


app = FastAPI(
    lifespan=lifespan,
    title=settings.title,
    version=settings.version,
    description=api_description,
    license_info={
        "name": "Apache 2.0",
        "identifier": "Apache-2.0",
    },
    swagger_ui_parameters={"defaultModelsExpandDepth": -1, "persistAuthorization": True},
    openapi_tags=[
        {"name": "user", "description": "Регистрация и авторизация пользователей"},
        {"name": "token", "description": "Работа с токенами"},
        {
            "name": "code",
            "description": "Запрос кодов подтверждения для активации аккаунтов",
        },
        {"name": "password", "description": "Сброс и смена паролей"},
        {"name": "session", "description": "Управлениями сессиями пользователей"},
    ],
    debug=settings.debug,
)


app.include_router(router, prefix="/v1")

celery = Celery(__name__, config_source="app.celeryconfig")

if settings.auth_sentry_dsn:
    import sentry_sdk

    sentry_sdk.init(
        dsn=settings.auth_sentry_dsn,
        traces_sample_rate=1.0,
        profiles_sample_rate=1.0,
        send_default_pii=True,
        environment=settings.environment,
        integrations=[
            RedisIntegration(),
            CeleryIntegration(),
        ],
    )


@app.exception_handler(KapibaraException)
async def base_exception_handler(request: Request, exc: KapibaraException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": [{"type": exc.error_type, "msg": exc.message}]},
    )


@app.exception_handler(ValidationError)
async def validation_error_handler(request: Request, exc: ValidationError):
    return JSONResponse(
        status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        content={"detail": [{"type": "server_error", "msg": str(exc)}]},
    )


@app.get("/health/")
async def health(
    db: Session = Depends(deps.get_db), redis: Redis = Depends(deps.get_redis)
):
    redis_key = "health"
    redis_flag = b"ok"
    await redis.set(redis_key, redis_flag, ex=2)
    assert redis_flag == await redis.get(redis_key)  # nosec B101

    result = db.execute(text("select 1"))
    assert result.first()[0] == 1  # nosec B101

    return {"status": "I'm OK"}


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_headers=["*"],
    allow_methods=["*"],
)
