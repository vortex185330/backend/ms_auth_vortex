from typing import Annotated

from fastapi import Body, Header

from app.core.settings import settings

UserAgent = Annotated[str | None, Header()]

RefreshToken = Annotated[str, Body(embed=True)]

TelegramHeader = Annotated[
    str | None, Header(alias=settings.confirmation_code_internal_token_header)
]
