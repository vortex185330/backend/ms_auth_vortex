.PHONY: build run rebuild-and-restart bash shell migrate test

build:
	docker compose build --no-cache

run:
	docker compose up

rebuild-and-restart:
	docker compose down --rmi all && docker compose up -d --force-recreate

bash:
	docker compose exec kapibara-auth bash

shell:
	docker compose exec kapibara-auth ipython

migrate:
	docker compose exec kapibara-auth alembic upgrade head

test:
	docker compose run kapibara-auth pytest
