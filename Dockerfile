FROM python:3.11-slim-buster AS prod
COPY requirements/base.txt /tmp/base.txt
RUN pip install -r /tmp/base.txt
WORKDIR /app
ADD https://kapibucket-images-prod.fra1.cdn.digitaloceanspaces.com/system/geo/GeoLite2-City.mmdb ./app/core/geo/
COPY ./app ./app
CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "3333"]

FROM prod AS dev
COPY requirements/dev.txt /tmp/dev.txt
RUN pip install -r /tmp/dev.txt && \
    apt update && apt install git gcc -y && apt clean
ENV PYTHONPATH /app
CMD ["uvicorn", "app.main:app", "--reload", "--proxy-headers", "--host", "0.0.0.0", "--port", "3333"]
